<?php

use Illuminate\Support\Facades\Route;

//Está correcto si en estos links se ve una pagina en azul con cuadros:

//Proyecto está funcionando en el servidor de XAMPP:
//http://127.0.0.1/fernando_torres/public/

//servidor de artisan para usarlo de manera local:
//http://127.0.0.1:8000/


//ORIGINAL -->>
Route::get('/', function () {
    return view('welcome');
});

//EJERCICIOS Web Routes / Registro de rutas web de Route Service Provider / Declaracion de rutas

//EJERCICIO 1
//http://127.0.0.1/fernando_torres/public/
//http://127.0.0.1:8000/
Route::get('/', function () {
    return "Bienvenido";
    });


//EJERCICIO 2
//http://127.0.0.1/fernando_torres/public/cursos
//http://127.0.0.1:8000/cursos
Route::get('cursos', function () {
    return "Bienvenido a la página de cursos";
});


//EJERCICIO 3
//http://127.0.0.1:8000/sesiones/1
Route::get('sesiones/{sesion}', function ($sesion) {
    return "Ingresaste a la sesión número: $sesion";
    });


//EJERCICIO 4
//http://127.0.0.1:8000/alumnos/xxx/yyy
Route::get('alumnos/{carrera}/{asignatura}',
    function ($carrera, $asignatura) {
    return "Bienvenido a la carrera: $carrera,
    a la asignatura $asignatura";
});


//EJERCICIO 5
//http://127.0.0.1:8000/alumnos/informatica
Route::get('alumnos/{carrera}/{asignatura?}',
    function ($carrera, $asignatura = null) {
    return "Bienvenido a $carrera $asignatura";
});


//EJERCICIO 6
//http://127.0.0.1:8000/alumnos/informatica/programacion
Route::get('alumnos/{carrera}/{asignatura}', function ($carrera, $asignatura = null) {if ($asignatura)
    {
        return "Bienvenido a la carrera: $carrera, asignatura: $asignatura";
    } else {
        return "Bienvenido a la carrera: $carrera";
    }
});



Route::get('/', function () {
    return view('welcome');
});


Route::get('/index', [TiendaController::class, 'index'
]);

Route::get('/crear', [TiendaController::class, 'crear'
]);

Route::get('/mostrar/{producto?}', [TiendaController::class, 'mostrar'
]);   

/*
*/


Route::get('/', function () {
    return view('welcome');
});