<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Abogado>
 */
class AbogadoFactory extends Factory
{
    /**s
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $faker = Faker::create();
        return [
            'nombre' => $faker->firstName(),
            'apellido' => $faker->lastName(),
            'fecha_registro' => $faker->date($format = 'Y-m-d', $max = 'now'),
        ];
    }
}
