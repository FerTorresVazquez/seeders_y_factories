<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Prioridad;
use Faker\Factory as Faker;

class EmpresaFactory extends Factory
{

    public function definition(): array
    {
        $prioridades = Prioridad::all();
        return [
            'id' =>
            'nombre' => $faker->company(),
            'descripcion' => $faker->bs(),
            'telefono' => $faker ->numerify('55########'),
            'correo' => $faker ->safeEmail(),
            'direccion' => $faker ->adress(),
            'id_prioridad' => $faker ->randomElement($prioridades)->id,
        ];
    }
}

$factory->state(Empresa::class, 'sin correo', function (Faker $faker)   {
    $prioridades = Prioridad::all();
    return [
        'id' =>
        'nombre' => $faker->company(),
        'descripcion' => $faker->bs(),
        'telefono' => $faker ->numerify('55########'),
        'correo' => 'null',
        'direccion' => $faker ->adress(),
        'id_prioridad' => $faker ->randomElement($prioridades)->id,
    ];
});