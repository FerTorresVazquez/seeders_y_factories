<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EmpresaAbogado>
 */
class EmpresaAbogadoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        /*ESTOS SON ARREGLOS*/
        $empresas App\Empresa::all()
        $abogados App\Abogado::all()

        return [

            'id_abogado' => $faker->randomElement('$abogados')->id;
            'id_empresa' => $faker->randomElement('$empresas')->id;
            
        ];
    }
}
