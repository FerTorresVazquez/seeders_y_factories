<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);


        /*ESTO ES LO QUE PUSO EN EL VIDEO Y QUE DEBIO FUNCIONAR:
        factory(App\Abogado::class, 10)->create();
        factory(App\Empresa::class, 10)->create();
        factory(App\EmpresaAbogado::class, 10)->create();
        */
        DB::table('abogados')->insert([
            'nombre' => 'Juan',
            'apellido' => 'Hernández',
            'fecha_registro' => '2003-12-31',
        ]);

        
    }
}
