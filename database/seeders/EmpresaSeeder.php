<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        factory(App\Empresa::class, 50)->create();
    }
}
